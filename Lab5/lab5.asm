%include '../util.asm'

global start

section .bss
	string: 	resb	256
	buf: 		resb	16
	arr:		resb 	128
	fdscrpt:	resd	1
	size:		resb	1
	n:			resb	1

section .data
	inputfile:		db	"./input.txt",0
	outputfile:		db	"./output.txt",0
	tab:			db  9,0

section .text

start:
	mov 	eax, O_READONLY
	mov 	esi, inputfile
	call 	openfile
	mov 	[fdscrpt], eax

	push 	dword 255
	push 	string
	push 	dword [fdscrpt]
	sub 	esp, 4
	mov 	eax, 3
	int 	80h
	add 	esp, 16

	mov 	byte [string + eax], 0 

	push 	dword [fdscrpt]
	sub 	esp, 4
	mov 	eax, 6
	int 	80h
	add 	esp, 16

	mov 	eax, 0 ; tabs
	mov 	ebx, 0 ; newlines
	mov 	ecx, 0
	
	.countdimensions:
		cmp 	byte [string + ecx], 9
		jne 	.next
		inc 	eax
		jmp 	.dimend

		.next:
		cmp 	byte [string + ecx], 10
		jne 	.dimend
		inc 	ebx

		.dimend:
		inc 	ecx
		cmp 	byte [string + ecx], 0
		jne 	.countdimensions

	add 	eax, ebx
	mov 	byte [size], al
	push 	eax
	div 	bx

	mov 	byte [n], al

	call 	zeroall

	.toarray:
		cmp 	byte[string + ecx], '0'
		jl  	.writetoarray

		mov 	al, byte[string + ecx]
		mov 	byte[buf + edx], al
		inc 	edx
		jmp 	.post

		.writetoarray:
			mov 	byte[buf + edx + 1], 0
			mov 	esi, buf
			call 	atoi
			mov 	byte[arr + ebx], al
			inc 	ebx
			mov 	edx, 0

		.post:
		inc 	ecx
		cmp 	byte [string + ecx], 0
		jne 	.toarray

	pop 	eax
	mov 	ecx, eax
	mov 	ebx, 2
	div 	bl
	mov 	edx, ecx

	mov 	ecx, eax
	mov 	eax, 0
	mov 	ebx, 0

	.invert:
		dec 	edx
		mov 	al, byte [arr + ebx]
		xchg	al, byte [arr + edx]
		mov 	byte [arr + ebx], al
		inc 	ebx
		loop 	.invert

	mov 	eax, O_CREATE
	mov 	esi, outputfile
	call 	openfile
	mov 	[fdscrpt], eax

	mov 	edi, buf
	mov 	eax, 0
	mov 	ecx, 0
	mov 	ebx, 0
	.print:
		mov 	al, byte[arr + ecx]
		call 	itoa
		mov 	esi, buf

		mov 	esi, buf
		call 	print
		mov 	eax, [fdscrpt]
		call 	fprint

		inc 	ebx
		cmp 	bl, byte[n]
		je  	.nextline

		mov 	esi, tab
		call 	print
		mov 	eax, [fdscrpt]
		call 	fprint

		jmp 	.postprint

		.nextline:
			mov 	esi, nl
			mov 	eax, [fdscrpt]
			call 	fprint
			call 	newline

			mov 	ebx, 0

		.postprint:
		inc 	ecx
		cmp 	cl, byte[size]
		jl  	.print

	mov 	eax, dword[fdscrpt]
	call 	closefile

	call 	exit