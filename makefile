EXECUTABLE = lab
SOURCE = $(shell ls *asm | sed 1q)
OBJ = $(shell echo $(SOURCE) | cut -f 1 -d '.').o
AS = /usr/local/bin/nasm
ASFLAGS = -f macho
LD = ld
LDFLAGS = -macosx_version_min 10.7.0 -o $(EXECUTABLE) $(OBJ)
DBG = lldb

.PHONY: $(EXECUTABLE)

run: link $(EXECUTABLE)
	@./$(EXECUTABLE)

build: ../util.asm
	@$(AS) $(ASFLAGS) $(SOURCE)

link: build $(OBJ)
	@$(LD) $(LDFLAGS)

debug: link $(EXECUTABLE)
	@$(DBG) $(EXECUTABLE)

clean:
	@rm -f $(EXECUTABLE) 2> /dev/null
	@rm -f *.o 2> /dev/null