; /usr/local/bin/nasm -f macho 32.asm && ld -macosx_version_min 10.7.0 -o 32 32.o && ./32


; Если a ^ 2 > c-b
;     Если c * b < d / b то
;         Результат = a OR b
;     Иначе
;         Результат = с
; Иначе
;     Результат = с * a - b

global start

section .data

a: 		dw 		2
b: 		dw 		4
c: 		dw 		7
d: 		dw 		8

; a: 		dw 		2
; b: 		dw 		3
; c: 		dw 		5
; d: 		dw 		150

; a: 		dw 		2
; b: 		dw 		4
; c: 		dw 		9
; d: 		dw 		8

section .text
start:
	mov 	ax, [a]
	mul 	ax

	mov 	bx, [c]
	sub 	bx, [b]

	cmp 	ax, bx
	jle 	else1

	cond1:
		mov 	bx, [b]
		mov 	cx, [c]
		mov 	dx, 0

		imul 	cx, bx
		mov 	ax, [d]
		div 	bx

		cmp 	cx, ax
		jge 	else2

		cond2:
			mov 	ax, [a]
			or 		ax, bx
			jmp 	exit

		else2:
			mov 	ax, [c]
			jmp 	exit

	else1:
		mov 	ax, [a]
		imul 	ax, [c]
		sub 	ax, [b]
		jmp 	exit

exit:
	push	dword 0
	mov 	eax, 1
	sub 	esp, 4
 	int 	0x80
