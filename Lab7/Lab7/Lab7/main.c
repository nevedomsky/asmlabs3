﻿#include <windows.h>

#define CLOSE 101
#define MOVE 111
#define CLEAR 1000

WORD xPos, yPos;

int nTimerID;

const int picw = 200;
const int pich = 80;
const int margin = 16;
const int speed = 2;

LPCWSTR image = L"..\\picture.bmp";

int isMoving = 0, isDrawn = 0, direction = 0;
int fieldw = 0, fieldh = 0;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void DrawBorder(HDC hDC) {
	MoveToEx(hDC, margin, margin, NULL);
	LineTo(hDC, margin + fieldw, margin);
	LineTo(hDC, margin + fieldw, margin + fieldh);
	LineTo(hDC, margin, margin + fieldh);
	LineTo(hDC, margin, margin);
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow) {
	HWND hMainWnd;
	MSG msg;
	WNDCLASSEX wc;
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = L"ClassWnd";
	wc.cbWndExtra = 0;
	wc.cbClsExtra = 0;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hInstance = hInst;

	if (!RegisterClassEx(&wc)) {
		MessageBox(NULL, L"Cannot register the class!", L"Error", MB_OK);
		return 0;
	}

	int ok = 1;
	hMainWnd = CreateWindow(wc.lpszClassName, L"Lab 7", WS_OVERLAPPED, 100, 100, 1040, 824, NULL, NULL, hInst, NULL);
	ok *= (int)hMainWnd;
	int btnStyle = WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON | BS_FLAT;
	ok *= (int)CreateWindow(L"BUTTON", L"Exit",  btnStyle,  16, 720, 320, 48, hMainWnd, (HMENU)CLOSE, hInst, 0);
	ok *= (int)CreateWindow(L"BUTTON", L"Move",  btnStyle, 352, 720, 320, 48, hMainWnd, (HMENU)MOVE,  hInst, 0);
	ok *= (int)CreateWindow(L"BUTTON", L"Clear", btnStyle, 688, 720, 320, 48, hMainWnd, (HMENU)CLEAR, hInst, 0);

	if (!ok) {
		MessageBox(NULL, L"Cannot create the window!", L"Error", MB_OK);
		return 0;
	}

	ShowWindow(hMainWnd, nCmdShow);
	UpdateWindow(hMainWnd);

	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	HDC hDC, hCompatibleDC;
	HANDLE hBitmap, hOldBitmap;
	RECT Rect;
	BITMAP Bitmap;

	switch (uMsg) {
		case WM_CREATE:
			GetClientRect(hWnd, &Rect);
			fieldw = Rect.right - Rect.left - margin * 2;
			fieldh = Rect.bottom - Rect.top - margin - 80;
			break;

		case WM_LBUTTONDOWN:
			if (isDrawn) break;

			xPos = LOWORD(lParam);
			yPos = HIWORD(lParam);

			if (xPos < margin || yPos < margin || xPos > fieldw + margin || yPos > fieldh + margin) break;

			if (xPos > fieldw + margin - picw / 2) xPos = fieldw + margin - picw / 2;
			else if (xPos < picw / 2 + margin) xPos = picw / 2 + margin;
			if (yPos > fieldh + margin - pich / 2) yPos = fieldh + margin - pich / 2;
			else if (yPos < pich / 2 + margin) yPos = pich / 2 + margin;

			isDrawn = 1;

			hDC = GetDC(hWnd);
			hBitmap = LoadImage(NULL, image, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			if (!hBitmap) {
				MessageBox(hWnd, L"Could not find the file!", L"Error", MB_OK);
				return 0;
			}
			GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
			hCompatibleDC = CreateCompatibleDC(hDC);
			hOldBitmap = SelectObject(hCompatibleDC, hBitmap);

			BitBlt(hDC, xPos - picw / 2, yPos - pich / 2, picw, pich, hCompatibleDC, 0, 0, SRCCOPY);

			DrawBorder(hDC);

			DeleteObject(hBitmap);
			DeleteDC(hCompatibleDC);
			ReleaseDC(hWnd, hDC);

			break;

		case WM_TIMER:
			hDC = GetDC(hWnd);
			hBitmap = LoadImage(NULL, image, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			GetObject(hBitmap, sizeof(BITMAP), &Bitmap);
			hCompatibleDC = CreateCompatibleDC(hDC);
			hOldBitmap = SelectObject(hCompatibleDC, hBitmap);

			xPos += speed * (((direction + 1) & 2) - 1);
			yPos += speed * (((direction) & 2) - 1);

			if (xPos > margin + fieldw - picw / 2 - speed ||
				yPos > margin + fieldh - pich / 2 - speed ||
				xPos < margin + picw / 2 + speed ||
				yPos < margin + pich / 2 + speed
				) {
				direction++;
			}

			BitBlt(hDC, xPos - picw / 2, yPos - pich / 2, picw, pich, hCompatibleDC, 0, 0, SRCCOPY);

			DrawBorder(hDC);

			DeleteObject(hBitmap);
			DeleteDC(hCompatibleDC);
			ReleaseDC(hWnd, hDC);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam)) {
				case CLOSE:
					if (IDYES == MessageBox(hWnd, L"Are you sure?", L"Warning", MB_ICONQUESTION | MB_YESNO))
						DestroyWindow(hWnd);
					break;

				case MOVE:
					if (!isDrawn) {
						MessageBox(hWnd, L"The image hasn't been drawn!", L"Error", MB_OK);
						break;
					}

					if (isMoving) KillTimer(hWnd, nTimerID);
					else nTimerID = SetTimer(hWnd, 1, 1, NULL);

					isMoving ^= 1;
					break;

				case CLEAR:
					isDrawn = 0;
					KillTimer(hWnd, nTimerID);
					isMoving = 0;
					InvalidateRect(hWnd, 0, TRUE);
					UpdateWindow(hWnd);
					MessageBox(hWnd, L"Everything is cleared successfully!", L"Cleared", MB_OK);
					break;
			}
			break;

		case WM_CLOSE:
			DestroyWindow(hWnd);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}
