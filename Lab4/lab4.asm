%include '../util.asm'

; Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
; do eiusmod' tempor incididunt ut labore et dolore magna aliqua.
; Ut enim ad minim veniam, qui's nostrud exercitation ullamco
; laborisnisi ut aliquip ex - eac commodo consequat.

global start

section .bss
	string: 	resb	256
	ptrs: 		resb	64  ; pointers to the beginning of every word

section .data
	msgin:		db	"Input text (255 chars max): ",0
	msgout:		db	"Output: ",0

section .text

start:
	mov 	esi, msgin
	call 	println

	mov 	eax, 255
	mov 	edi, string
	call 	read

	mov 	eax, 0 ; char displacement
	mov 	ecx, 0 ; pointers count

	.loops:
		cmp 	byte [string + eax], 0
		je  	.loopend
		cmp 	byte [string + eax], ' '
		je  	.post

		mov 	byte [ptrs + ecx], al
		inc 	ecx

		.skipword:
			inc 	eax
			cmp 	byte [string + eax], 0
			je  	.loopend
			cmp 	byte [string + eax], ' '
			jne 	.skipword

		.post:
		inc 	eax
		jmp 	.loops
	.loopend:

	mov 	ebx, ecx
	.sort:
		dec 	ebx

		mov 	edx, 0
		.inner:
			mov 	esi, string
			mov 	eax, 0
			mov 	al, byte [ptrs + edx]
			add 	esi, eax

			mov 	edi, string
			mov 	eax, 0
			mov 	al, byte [ptrs + edx + 1]
			add 	edi, eax

			call 	strcmp
			cmp 	eax, 1
			jne 	.post2

			mov 	eax, 0
			mov 	al, byte [ptrs + edx]
			xchg 	al, byte [ptrs + edx + 1]
			mov 	byte [ptrs + edx], al

			.post2:
			inc 	edx
			cmp 	edx, ebx
			jl  	.inner

		cmp 	ebx, 0
		jg  	.sort

	call 	newline
	mov 	esi, msgout
	call 	println

	.write:
		mov 	esi, string
		mov 	eax, 0
		mov 	al, byte [ptrs + ebx]
		add 	esi, eax
		call 	printword
		call 	newline
		inc 	ebx
		loop 	.write

	call 	exit
