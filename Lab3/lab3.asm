; /usr/local/bin/nasm -f macho 32.asm && ld -macosx_version_min 10.7.0 -o 32 32.o && ./32

; Если a ^ 2 > c-b
;     Если c * b < d / b то
;         Результат = a OR b
;     Иначе
;         Результат = с
; Иначе
;     Результат = с * a - b

;	 a		b		 c		d
;	-4		5		-19		10

;	-4		5		19		-10

;	-4		-5		19		10


%include '../util.asm'

global start

section .bss
	buf:	resb	256
	vara:	resd	1
	varb:	resd	1
	varc:	resd	1
	vard:	resd	1

section .data
	msgerr:		db	"Number was out of range [-32768 , 32767]. Try again!",0
	msgin:		db	"Input data: ",0
	msgout:		db	"Output: ",0
	msgzerodiv:	db	"Cannot divide by Zero!",0

section .text

checkrange:
	mov 	esi, msgerr
	cmp 	eax, WORDMAX
	jg  	error
	cmp 	eax, WORDMIN
	jl  	error
	ret

start:
	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[vara], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[varb], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[varc], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[vard], eax

	br:

	mov 	eax, [vara]
	imul 	eax

	mov 	ebx, [varc]
	sub 	ebx, [varb]

	cmp 	eax, ebx
	jle 	else1

	cond1:
		mov 	ebx, [varb]
		cmp 	ebx, 0
		jne 	.correct
		mov 	esi, msgzerodiv
		call 	error

		.correct:
		mov 	ecx, [varc]
		mov 	edx, 0

		imul 	ecx, ebx
		mov 	eax, [vard]
		cdq
		idiv 	ebx

		cmp 	ecx, eax
		jge 	else2

		cond2:
			mov 	eax, [vara]
			or 		eax, ebx
			jmp 	finish

		else2:
			mov 	eax, [varc]
			jmp 	finish

	else1:
		mov 	eax, [vara]
		imul 	eax, [varc]
		sub 	eax, [varb]
		jmp 	finish

error:
	call 	println
	call 	exit

finish:
	mov 	esi, msgout
	call 	print

	mov 	edi, buf
	call 	itoa

	mov 	esi, edi
	call 	println

	call 	exit
