; /usr/local/bin/nasm -f macho 32.asm && ld -macosx_version_min 10.7.0 -o 32 32.o && ./32

; Если a ^ 2 > c-b
;     Если c * b < d / b то
;         Результат = a OR b
;     Иначе
;         Результат = с
; Иначе
;     Результат = с * a - b
%include '../util.asm'

global start

section .bss
	buf:	resb	256
	vara:	resw	1
	varb:	resw	1
	varc:	resw	1
	vard:	resw	1

section .data
	msgerr:		db	"Number was out of range [0, 65535]. Try again!",0
	msgin:		db	"Input data: ",0
	msgout:		db	"Output: ",0
	msgzerodiv:	db	"Cannot divide by Zero!",0

section .text

checkrange:
	mov 	esi, msgerr
	cmp 	eax, 65535
	jg  	error
	cmp 	eax, 0
	jl  	error
	ret

start:
	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[vara], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[varb], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[varc], eax

	mov 	esi, msgin
	call 	print

	call 	readint
	call 	checkrange
	mov 	[vard], eax

	br:

	mov 	ax, [vara]
	mul 	ax

	mov 	bx, [varc]
	sub 	bx, [varb]

	cmp 	ax, bx
	jle 	else1

	cond1:
		mov 	bx, [varb]
		cmp 	bx, 0
		jne 	.correct
		mov 	esi, msgzerodiv
		call 	error

		.correct:
		mov 	cx, [varc]
		mov 	dx, 0

		imul 	cx, bx
		mov 	ax, [vard]
		div 	bx

		cmp 	cx, ax
		jge 	else2

		cond2:
			mov 	ax, [vara]
			or 		ax, bx
			jmp 	finish

		else2:
			mov 	ax, [varc]
			jmp 	finish

	else1:
		mov 	ax, [vara]
		imul 	ax, [varc]
		sub 	ax, [varb]
		jmp 	finish

error:
	call 	println
	call 	exit

finish:
	mov 	esi, msgout
	call 	print

	mov 	edi, buf
	call 	itoa

	mov 	esi, edi
	call 	println

	call 	exit
